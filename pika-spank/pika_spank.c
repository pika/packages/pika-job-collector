/*****************************************************************************
 *
 *  Copyright (C) 2014 Technische Universitaet Dresden.
 *  Produced at Technische Universitaet Dresden.
 *  Written by
 *    Danny Rotscher <danny.rotscher@tu-dresden.de>
 *
 *  This file contains a spank plugin for SLURM.
 *
 *  This is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include <slurm/spank.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>

#define PLUGIN_NAME "spank: "

/*
 * All spank plugins must define this macro for the SLURM plugin loader.
 */
SPANK_PLUGIN(pika, 1);

/*  SPANK plugin operations. SPANK plugin should have at least one of
 *   these functions defined non-NULL.
 *
 *  Plug-in callbacks are completed at the following points in slurmd:
 *
 *   slurmd
 *        `-> slurmd_init()
 *        |
 *        `-> job_prolog()
 *        |
 *        | `-> slurmstepd
 *        |      `-> init ()
 *        |       -> process spank options
 *        |       -> init_post_opt ()
 *        |      + drop privileges (initgroups(), seteuid(), chdir())
 *        |      `-> user_init ()
 *        |      + for each task
 *        |      |       + fork ()
 *        |      |       |
 *        |      |       + reclaim privileges
 *        |      |       `-> task_init_privileged ()
 *        |      |       |
 *        |      |       + become_user ()
 *        |      |       `-> task_init ()
 *        |      |       |
 *        |      |       + execve ()
 *        |      |
 *        |      + reclaim privileges
 *        |      + for each task
 *        |      |     `-> task_post_fork ()
 *        |      |
 *        |      + for each task
 *        |      |       + wait ()
 *        |      |          `-> task_exit ()
 *        |      `-> exit ()
 *        |
 *        `---> job_epilog()
 *        |
 *        `-> slurmd_exit()
 *
 *   In srun only the init(), init_post_opt() and local_user_init(), and exit()
 *    callbacks are used.
 *
 *   In sbatch/salloc only the init(), init_post_opt(), and exit() callbacks
 *    are used.
 *
 *   In slurmd proper, only the slurmd_init(), slurmd_exit(), and
 *    job_prolog/epilog callbacks are used.
 *
 */

/* 
 *	typedef struct spank_handle * spank_t; --> defined in $(SLURM_INC_DIR)/slurm/spank.h
 *	struct spank_handle --> defined in $(SLURM_SRC_DIR)/src/common/plugstack.c
 */

static char pika_monitoring[8] = "1";

static int _pika_opt_process (int val,
                              const char *optarg,
                              int remote);

/*
 * Provide --pika-no-monitoring option to srun:
 */
struct spank_option spank_option_array[] =
{
  { "pika-no-monitoring", NULL,
    "Switch to disable PIKA monitoring", 0, 0,
    (spank_opt_cb_f) _pika_opt_process
  },
  SPANK_OPTIONS_TABLE_END
};

int slurm_spank_init(spank_t spank, int ac, char *argv[])
{
  spank_option_register(spank, &spank_option_array[0]);

  return 0;
}

int slurm_spank_init_post_opt(spank_t spank,
                              int ac,
                              char *argv[])
{
  spank_context_t context = spank_context();
  if ((context == S_CTX_LOCAL) || (context == S_CTX_ALLOCATOR)) {
    spank_job_control_setenv(spank, "PIKA_MONITORING", pika_monitoring, 1);
  }

  return 0;
}

static int _pika_opt_process (int val,
                               const char *optarg,
                               int remote)
{
  switch(val) {
    case 0: sprintf(pika_monitoring, "%s", "0"); break;
    default: break;
  }

  return ESPANK_SUCCESS;
}
