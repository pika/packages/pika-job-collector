# PIKA Spank Plugin

## Requirements

The following requirements are needed:
- Slurm installed on the host
- rpmbuild (rpm package name "rpm-build")
- gcc and make

## Build

```
make rpm
```

## Install

```
rpm -i rpmbuild/RPMS/x86_64/pika-spank*.rpm
or
yum localinstall rpmbuild/RPMS/x86_64/pika-spank*.rpm
or
dnf localinstall rpmbuild/RPMS/x86_64/pika-spank*.rpm
```

## Slurm Integration

```
vi /etc/slurm/plugstack.conf
[...]
optional /usr/lib64/slurm/pika_spank.so
[...]
```

**Hint**: If plugstack.conf does not exist, create it in the same directory as slurm.conf.


Example prolog script:

```
#!/bin/bash

PIKA_SPANK_LOG="/tmp/pika_spank.log"

(
if [[ "${SPANK_PIKA_MONITORING}" == "1" ]]; then
  echo "PIKA monitoring enabled"
  echo "systemctl start pika-collectd"
  exit 0
elif  [[ "${SPANK_PIKA_MONITORING}" == "0" ]]; then
  echo "PIKA monitoring disabled"
  echo "systemctl stop pika-collectd"
  exit 0
fi
) &> ${PIKA_SPANK_LOG}
```

## Submit test job
Use the flag `--pika-no-monitoring` when submitting a job.

Example:
```sh
vi run.sh
#!/bin/bash

#SBATCH --job-name=JOB_NAME
#SBATCH --nodes=1
#SBATCH --pika-no-monitoring
...
```



