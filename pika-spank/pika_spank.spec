Name:		pika-spank	
Version:	0.1
Release:	1%{?dist}
Summary:	slurm plugin spank cloud

Group:		System Environment/Base
License:	GPL
URL:		https://gitlab.hrz.tu-chemnitz.de/hpcsupport/slurm
Source0:	pika_spank.tar.gz

#BuildRequires:	
#Requires:	

%description
A SLURM spank plugin for launching vms and container


%prep
%setup -q -n pika_spank


%build
make 


%install
mkdir -p ${RPM_BUILD_ROOT}/usr/lib64/slurm
make install SLURM_ROOT_DIR=${RPM_BUILD_ROOT}/usr


%files
/usr/lib64/slurm/pika_spank.so


#%changelog

