Name: pika
Summary: Center-Wide and Job-Aware Cluster Monitoring
Version: 1.0.0
Release: 1%{?dist}
License: GPL-3.0
Source: pika.tar.gz
Group: Development/Tools
Packager: Sebastian Oeste <sebastian.oeste@tu-dresden.de>
URL: https://gitlab.hrz.tu-chemnitz.de/pika/monitoring

Requires: collectd
Requires: likwid
Requires: pika-collectd-likwid-plugin
Requires: clustershell
Requires: mariadb

%description
Pika is a software suite for Center-Wide and Job-Aware Cluster Monitoring,
the best you can have since cutted bread.


%prep
%setup -q -n pika

%install
# install configs
install -Dp -m0755 pika.conf %{buildroot}/etc/pika/pika-control/pika.conf
install -Dp -m0700 pika_access %{buildroot}/etc/pika/pika-control/pika_access

# install slurm stuff
install -Dp -m0755 epilog.sh %{buildroot}/etc/pika/pika-control/slurm/epilog.sh
install -Dp -m0755 prolog.sh %{buildroot}/etc/pika/pika-control/slurm/prolog.sh
install -Dp -m0755 pika_update_metadata_epilog_include.sh %{buildroot}/etc/pika/pika-control/slurm/utils/pika_update_metadata_epilog_include.sh
install -Dp -m0755 pika_utils.sh %{buildroot}/etc/pika/pika-control/slurm/utils/pika_utils.sh
install -Dp -m0755 pika_slurm_env_epilog.sh %{buildroot}/etc/pika/pika-control/slurm/utils/pika_slurm_env_epilog.sh
install -Dp -m0755 pika_save_metadata_prolog_include.sh %{buildroot}/etc/pika/pika-control/slurm/utils/pika_save_metadata_prolog_include.sh
install -Dp -m0755 pika_slurm_env_prolog.sh %{buildroot}/etc/pika/pika-control/slurm/utils/pika_slurm_env_prolog.sh


%files
#%doc README.md flow_graph.png flow_graph.svg
#%{_datadir}/pika/pika-control/pika.conf
/etc/pika/pika-control/pika.conf
/etc/pika/pika-control/pika_access

/etc/pika/pika-control/slurm/epilog.sh
/etc/pika/pika-control/slurm/prolog.sh
/etc/pika/pika-control/slurm/utils/pika_update_metadata_epilog_include.sh
/etc/pika/pika-control/slurm/utils/pika_utils.sh
/etc/pika/pika-control/slurm/utils/pika_slurm_env_epilog.sh
/etc/pika/pika-control/slurm/utils/pika_save_metadata_prolog_include.sh
/etc/pika/pika-control/slurm/utils/pika_slurm_env_prolog.sh
