## Set database credentials


```sh
cp pika_access_template pika_access
vi pika_access
#!/bin/bash

#connection data for MariaDB
export MARIADB_HOST="YOUR-HOST"
export MARIADB_PORT=3306
export MARIADB_USER="admin"
export MARIADB_PASSWORD="YOUR-PASSWORD"
export MARIADB_DATABASE="pika"
```

## Build RPM

To build the rpm package just run `make rpm`. Note that you need to have `rpmbuild` installed.

## Install RPM
```sh
rpm -i rpmbuild/RPMS/x86_64/pika-1.0.0-1.x86_64.rpm
or
yum localinstall rpmbuild/RPMS/x86_64/pika-1.0.0-1.x86_64.rpm
or
dnf localinstall rpmbuild/RPMS/x86_64/pika-1.0.0-1.x86_64.rpm
```

## Configuration
Set the correct paths of prolog and epilog in slurm.conf.

Example:
```sh
vi /etc/slurm/slurm.conf
/etc/pika/pika-control
Epilog=/etc/pika/pika-control/slurm/epilog.sh
Prolog=/etc/pika/pika-control/slurm/prolog.sh
```

**Hint:** You can also call the PIKA prolog and epilog scripts from your own prolog and epilog scripts.

## Restart the slurm daemon
```sh
systemctl restart slurmd
```
