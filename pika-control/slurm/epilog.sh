#!/bin/bash

# (1) Get PIKA environment variables
source /etc/pika/pika-control/pika.conf

PIKA_JOB_ID=${SLURM_JOB_ID}
PIKA_JOB_NODELIST=${SLURM_NODELIST}

# (2) Check for debug file
if [ "${PIKA_DEBUG}" == "1" ]; then
  mkdir -p ${PIKA_LOGPATH}/pika_debug
  DEBUG_PATH=${PIKA_LOGPATH}/pika_debug/pika_${PIKA_JOB_ID}
  echo -e "\nStart epilog debugging..." >> $DEBUG_PATH 2>&1
else
  DEBUG_PATH=/dev/null
fi

# (3) Determine master node
MASTER_NODE=`echo ${PIKA_JOB_NODELIST} | nodeset -e | cut -d ' ' -f 1 | cut -d. -f1`
echo -e "\nMASTER_NODE=$MASTER_NODE" >> $DEBUG_PATH 2>&1

# this node's name
PIKA_HOSTNAME=$(hostname | cut -d. -f1)
echo "PIKA_HOSTNAME=$PIKA_HOSTNAME" >> $DEBUG_PATH 2>&1

# (4) Update job metadata
source ${PIKA_ROOT}/slurm/utils/pika_update_metadata_epilog_include.sh >> $DEBUG_PATH 2>&1

echo -e "\nEpilog finished sucessfully!" >> $DEBUG_PATH 2>&1
exit 0
