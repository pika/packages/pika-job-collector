#!/bin/bash

if [ "${PIKA_HOSTNAME}" = "${MASTER_NODE}" ]; then

  # get utility functions
  source ${PIKA_ROOT}/slurm/utils/pika_utils.sh >> $DEBUG_PATH 2>&1

  # Get job data from SLURM
  source ${PIKA_ROOT}/slurm/utils/pika_slurm_env_epilog.sh >> $DEBUG_PATH 2>&1

  JOB_DURATION=$((PIKA_JOB_END - PIKA_JOB_START))
  echo -e "\nJOB_DURATION=${JOB_DURATION}" >> $DEBUG_PATH 2>&1

  # update job data in mariadb
  if [ "${JOB_DURATION}" -lt "60" ]; then
    SQL_QUERY="DELETE FROM job_data WHERE job_id=${PIKA_JOB_ID} AND start_time=${PIKA_JOB_START} AND partition_name='${PIKA_JOB_PARTITION}'"
  else
    SQL_QUERY="UPDATE job_data SET job_state='${PIKA_JOB_STATUS}',end_time=${PIKA_JOB_END} "
    SQL_QUERY+="WHERE job_id=${PIKA_JOB_ID} AND start_time=${PIKA_JOB_START} AND partition_name='${PIKA_JOB_PARTITION}'"
  fi

  # check if mysql is installed
  MYSQL_CHECK=`command -v mysql`
  if [ -z "${MYSQL_CHECK}" ]; then
    echo -e "\nMYSQL client not found! Cannot write into database!" >> $DEBUG_PATH 2>&1
  else
    mysql_command "${SQL_QUERY}"
  fi

fi

