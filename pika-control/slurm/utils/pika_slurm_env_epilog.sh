#!/bin/bash

PIKA_JOB_ID=${SLURM_JOB_ID}
PIKA_JOB_PARTITION=${SLURM_JOB_PARTITION}

# get remaining data from SLURM scontrol show job ${SLURM_JOB_ID} -d
slurm_data=$(scontrol show job ${SLURM_JOB_ID} -d)

PIKA_JOB_STATUS=$(echo $slurm_data | awk -F 'JobState=' '{print $2}' | awk '{print $1}')

start_time=$(echo $slurm_data | awk -F 'StartTime=' '{print $2}' | awk '{print $1}')
start_time_seconds=$(date -d "$start_time" +"%s")
PIKA_JOB_START=$start_time_seconds

end_time=$(echo $slurm_data | awk -F 'EndTime=' '{print $2}' | awk '{print $1}')
end_time_seconds=$(date -d "$end_time" +"%s")
PIKA_JOB_END=$end_time_seconds
