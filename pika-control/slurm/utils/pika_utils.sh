#!/bin/bash

function mysql_command()
{
  local MYSQL_COMMAND="mysql --connect_timeout=10 -u ${MARIADB_USER} -p${MARIADB_PASSWORD} -D ${MARIADB_DATABASE} -h ${MARIADB_HOST} -s -N -e"
  echo -e "\n${MYSQL_COMMAND} \"${1}\"" >> $DEBUG_PATH 2>&1
  result=$(${MYSQL_COMMAND} "${1}" 2>> $DEBUG_PATH)
  if [ $? -ne 0  ]; then
    echo -e "Error in MySQL Query.\nexit" >> $DEBUG_PATH 2>&1
    return
  fi
  echo $result
}

function send_email()
{
  # $1 - subject
  # $2 - filename printed as email text
  if [ -f ${PIKA_ROOT}/pika_emails ]; then
    while IFS= read -r line; do
      [[ "$line" =~ ^#.*$ ]] && continue
      mail -S smtp=mailin5.zih.tu-dresden.de -s "$1" -r $HOSTNAME@taurus.hrsk.tu-dresden.de $line < $2
    done < ${PIKA_ROOT}/pika_emails
  fi
}
