#!/bin/bash

if [ "${PIKA_HOSTNAME}" = "${MASTER_NODE}" ]; then

  # get utility functions
  source ${PIKA_ROOT}/slurm/utils/pika_utils.sh >> $DEBUG_PATH 2>&1

  # Get job data from SLURM
  source ${PIKA_ROOT}/slurm/utils/pika_slurm_env_prolog.sh >> $DEBUG_PATH 2>&1

  # save all metadata in job table
  echo -e "\nGet number of nodes" >> $DEBUG_PATH 2>&1
  JOB_NUM_NODES=$(nodeset -c ${PIKA_JOB_NODELIST}  2>> $DEBUG_PATH)
  
  if [ "$JOB_NUM_NODES" == "" ]; then
    echo -e "\nCould not determine number of nodes. Set to 0." >> $DEBUG_PATH 2>&1
    JOB_NUM_NODES=0
  else
    echo "JOB_NUM_NODES=$JOB_NUM_NODES" >> $DEBUG_PATH 2>&1
  fi

  # check if job is part of an array job
  JOB_ARRAY_ID=0
  if [ "${PIKA_JOB_ARRAY_ID}" != "None" ]; then
    JOB_ARRAY_ID=${PIKA_JOB_ARRAY_ID}
  fi

  # check if job name is too long (maximum length is 256)
  chrlen=${#PIKA_JOB_NAME}
  echo "Length of job name: $chrlen" >> $DEBUG_PATH 2>&1

  if [ $chrlen -gt 252 ]; then
    echo "Truncate job name string..." >> $DEBUG_PATH 2>&1  
    PIKA_JOB_NAME="${PIKA_JOB_NAME:0:252}..."
  fi

  # save all nodes for exclusive jobs (node number > 1) in cpu list in order to search jobs by a specific node
  if [ $PIKA_JOB_EXCLUSIVE -eq 1 ] && [ $JOB_NUM_NODES -gt 1 ]; then
    PIKA_JOB_CPUS_ALLOCATED=`echo ${PIKA_JOB_NODELIST} | nodeset -e`
  fi

  # create sql statement
  SQL_QUERY="INSERT INTO job_data "
  SQL_QUERY+="(job_id,user_name,project_name,job_state,node_count,node_list,core_list,core_count,submit_time,start_time,job_name,time_limit,partition_name,exclusive,property_id,array_id) "
  SQL_QUERY+="VALUES ('${PIKA_JOB_ID}','${PIKA_JOB_USER}','${PIKA_JOB_ACCOUNT}','running','${JOB_NUM_NODES}','${PIKA_JOB_NODELIST}','${PIKA_JOB_CPUS_ALLOCATED}','${PIKA_JOB_NUM_CORES}','${PIKA_JOB_SUBMIT}','${PIKA_JOB_START}','${PIKA_JOB_NAME}','${PIKA_JOB_WALLTIME}','${PIKA_JOB_PARTITION}','${PIKA_JOB_EXCLUSIVE}','${SPANK_PIKA_MONITORING}','${JOB_ARRAY_ID}')"

  # check if mysql is installed
  MYSQL_CHECK=`command -v mysql`
  if [ -z "${MYSQL_CHECK}" ]; then
    echo -e "\nMYSQL client not found! Cannot write into database!" >> $DEBUG_PATH 2>&1
  else
    mysql_command "${SQL_QUERY}"
  fi

fi
