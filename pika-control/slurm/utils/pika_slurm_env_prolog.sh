#!/bin/bash

function convert_to_seconds() {
    local time_string="$1"
    local days=0
    local hours=0
    local minutes=0
    local seconds=0

    # Check if the time string contains days
    if [[ $time_string == *-* ]]; then
        days=$(echo "$time_string" | cut -d '-' -f 1)
        time_string=$(echo "$time_string" | cut -d '-' -f 2)
    fi

    # Remove leading zeros
    time_string=$(echo "$time_string" | sed 's/^0*//')

    # Split the time string into components
    IFS=':' read -r -a components <<< "$time_string"
    local num_components=${#components[@]}

    # Extract hours, minutes, and seconds
    if [ "$num_components" -eq 3 ]; then
        hours=${components[0]}
        minutes=${components[1]}
        seconds=${components[2]}
    elif [ "$num_components" -eq 2 ]; then
        minutes=${components[0]}
        seconds=${components[1]}
    elif [ "$num_components" -eq 1 ]; then
        seconds=${components[0]}
    fi

    # Calculate total seconds
    local total_seconds=$((days * 86400 + hours * 3600 + minutes * 60 + seconds))
    echo "$total_seconds"
}

PIKA_JOB_ID=${SLURM_JOB_ID}
PIKA_JOB_NODELIST=${SLURM_NODELIST}
PIKA_JOB_USER=${SLURM_JOB_USER}
PIKA_JOB_EXCLUSIVE=1
PIKA_JOB_PARTITION=${SLURM_JOB_PARTITION}
PIKA_JOB_CPUS_ALLOCATED='n/a'
PIKA_JOB_ARRAY_ID='None'

# get remaining data from SLURM scontrol show job ${SLURM_JOB_ID} -d
slurm_data=$(scontrol show job ${SLURM_JOB_ID} -d)

if [[ $slurm_data == *"ArrayJobId="* ]]; then
  PIKA_JOB_ARRAY_ID=$(echo $slurm_data | awk -F 'ArrayJobId=' '{print $2}' | awk '{print $1}')
fi

PIKA_JOB_NAME=$(echo $slurm_data | awk -F 'JobName=' '{print $2}' | awk '{print $1}')
PIKA_JOB_STATUS=$(echo $slurm_data | awk -F 'JobState=' '{print $2}' | awk '{print $1}')
PIKA_JOB_ACCOUNT=$(echo $slurm_data | awk -F 'Account=' '{print $2}' | awk '{print $1}')
PIKA_JOB_NUM_CORES=$(echo $slurm_data | awk -F 'NumCPUs=' '{print $2}' | awk '{print $1}')

walltime=$(echo $slurm_data | awk -F 'TimeLimit=' '{print $2}' | awk '{print $1}')
walltime_seconds=$(convert_to_seconds "$walltime")
PIKA_JOB_WALLTIME=$walltime_seconds

submit_time=$(echo $slurm_data | awk -F 'SubmitTime=' '{print $2}' | awk '{print $1}')
submit_time_seconds=$(date -d "$submit_time" +"%s")
PIKA_JOB_SUBMIT=$submit_time_seconds

start_time=$(echo $slurm_data | awk -F 'StartTime=' '{print $2}' | awk '{print $1}')
start_time_seconds=$(date -d "$start_time" +"%s")
PIKA_JOB_START=$start_time_seconds
