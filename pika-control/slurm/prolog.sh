#!/bin/bash

####################################
# (1) Get PIKA environment variables
source /etc/pika/pika-control/pika.conf

# if SLURM_JOB_ID is of length zero
if [[ -z "${SLURM_JOB_ID}" ]]; then
  mkdir -p ${PIKA_LOGPATH}/pika_debug
  echo -e "\n SLURM_JOB_ID is not available. Exit prolog" > ${PIKA_LOGPATH}/pika_debug/slurm_job_id_not_available 2>&1
  env | grep SLURM >> ${PIKA_LOGPATH}/pika_debug/slurm_job_id_not_available 2>&1
  exit 0
else
  PIKA_JOB_ID=${SLURM_JOB_ID}
fi


####################################
# (2) Setup debugging
if [ ${PIKA_DEBUG} -eq 1 ]; then
  # delete debug files older than 7 days
  find ${PIKA_LOGPATH}/pika_debug/pika_* -mtime +7 -exec rm {} \;

  mkdir -p ${PIKA_LOGPATH}/pika_debug
  DEBUG_PATH=${PIKA_LOGPATH}/pika_debug/pika_${PIKA_JOB_ID}
  echo -e "Start prolog debugging..." > $DEBUG_PATH 2>&1
  chmod o+r $DEBUG_PATH
else
  DEBUG_PATH=/dev/null
fi

# print date
date >> $DEBUG_PATH 2>&1


# if SLURM_NODELIST is of length zero, exit prolog
if [[ -z "${SLURM_NODELIST}" ]]; then
  echo -e "\n SLURM_NODELIST is not available. Exit prolog" >> $DEBUG_PATH 2>&1
  exit 0
else
  PIKA_JOB_NODELIST=${SLURM_NODELIST}
fi

####################################
# (3) Determine master node
MASTER_NODE=`echo ${PIKA_JOB_NODELIST} | nodeset -e | cut -d ' ' -f 1 | cut -d. -f1`
echo -e "\nMASTER_NODE=$MASTER_NODE" >> $DEBUG_PATH 2>&1

# this node's name
PIKA_HOSTNAME=$(hostname | cut -d. -f1)
echo "PIKA_HOSTNAME=$PIKA_HOSTNAME" >> $DEBUG_PATH 2>&1

####################################
# (4) Start/stop pika-collectd
if [[ "${SPANK_PIKA_MONITORING}" == "1" ]]; then
  echo "PIKA monitoring enabled" >> $DEBUG_PATH 2>&1
  if (systemctl -q is-active pika-collectd) then
    echo "pika-collectd is already running." >> $DEBUG_PATH 2>&1
  else
    echo "systemctl start pika-collectd" >> $DEBUG_PATH 2>&1
    systemctl start pika-collectd >> $DEBUG_PATH 2>&1
  fi
elif [[ "${SPANK_PIKA_MONITORING}" == "0" ]]; then
  echo "PIKA monitoring disabled" >> $DEBUG_PATH 2>&1
  if (systemctl -q is-active pika-collectd) then
    echo "systemctl stop pika-collectd" >> $DEBUG_PATH 2>&1
    systemctl stop pika-collectd >> $DEBUG_PATH 2>&1
  fi
fi


# (5) Send job metadata to MariaDB 
source ${PIKA_ROOT}/slurm/utils/pika_save_metadata_prolog_include.sh >> $DEBUG_PATH 2>&1

echo -e "\nProlog finished successfully!" >> $DEBUG_PATH 2>&1
exit 0
