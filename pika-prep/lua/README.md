# Disabling job monitoring using the LUA script

User can disable PIKA monitoring via:

    #SBATCH --constraint=no_monitoring
    sbatch: error: --constraint=no_monitoring only available for node-exclusive jobs with --exclusive


Only works in addition with --exclusive flag:

    #SBATCH --exclusive
    #SBATCH --constraint=no_monitoring


## Instructions
1) Add the following code to job_submit.lua
```
function slurm_job_submit(job_desc, part_list, submit_uid)
        ...
        if ( job_desc.features ) then
		if ( string.find(job_desc.features, "no_monitoring") and job_desc.shared ~= 0) then
			slurm.log_info("slurm_job_submit: job from uid %u with constraint no_monitoring but not exclusive", job_desc.user_id )
			slurm.user_msg("--constraint=no_monitoring only available for node-exclusive jobs with --exclusive")
			return 2029 --- slurm.ERROR ESLURM_INVALID_GRES
        	end
        end

        ...
        return slurm.SUCCESS
end
```

If you are not yet using lua scripts in Slurm, use the template from `/etc/slurm/job_submit.lua.example`. However, this assumes that Slurm has been installed with lua support (lua-devel).


2) Add no_monitoring to nodeset specification in slum.conf

Example:
```
NodeName=c[1,2] Procs=24 Sockets=2 CoresPerSocket=12 ThreadsPerCore=2 RealMemory=62000 State=UNKNOWN Gres=gpu:6 Features=no_monitoring,dl
```

3) Restart slurm controller
```
systemctl restart slurmctld
```

# Starting and stopping the CollectD daemon in Slurm's prolog script on each compute node

This method assumes that the job metadata is already captured, e.g. via the Slurm PrEp plugin in the pika-server.

Copy `prolog.sh` to `/etc/slurm` and enable it in `/etc/slurm/slurm.conf`.

slurm.conf:

    Prolog=/etc/slurm/prolog.sh

Restart slurm daemon

    systemctl restart slurmd



