Summary: Slurm PREP PIKA Plugin
Name: slurm-prep-pika-plugin
Version: %(echo $SLURM_VERSION)
Release: 1%{?dist}
License: GPLv2
Source0: %{name}-%{version}.tar.gz
BuildRequires: slurm-devel
BuildRequires: util-linux
BuildRequires: librabbitmq-devel
BuildRequires: iniparser-devel
Requires: librabbitmq
Requires: iniparser

%description
Slurm PREP PIKA Plugin

%prep
%setup -q

%build
make

%install
install -Dp -m0755 prep_pika.so %{buildroot}/%{_libdir}/slurm/prep_pika.so
install -Dp -m0644 job_submit.lua.pika %{buildroot}/etc/pika/job_submit.lua.pika
install -Dp -m0644 rabbitmq.ini.example %{buildroot}/etc/pika/rabbitmq.ini.example
install -Dp -m0644 receive_rabbitmq.py %{buildroot}/etc/pika/receive_rabbitmq.py


%files
%{_libdir}/slurm/prep_pika.so
/etc/pika/job_submit.lua.pika
/etc/pika/rabbitmq.ini.example
/etc/pika/receive_rabbitmq.py

#%changelog
