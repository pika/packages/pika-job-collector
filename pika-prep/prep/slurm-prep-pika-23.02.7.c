/*****************************************************************************\
 *  PrEp script plugin, handles Prolog / Epilog /
 *  PrologSlurmctld / EpilogSlurmctld scripts
 *****************************************************************************
 *  Written by Frank Winkler, Benjamin Jurk and Mike Arnhold
 *
 *  LICENSE: LGPL-3.0-or-later
\*****************************************************************************/

#include <stdarg.h>
#include <unistd.h>
#include <inttypes.h>
#include <amqp.h>
#include <amqp_tcp_socket.h>
#include <iniparser/iniparser.h>

#include "slurm/slurm.h"
#include "slurm/slurm_errno.h"
#include "src/interfaces/prep.h"
#include "src/common/macros.h"
#include "src/common/xmalloc.h"
#include "src/common/xstring.h"
#include "src/common/log.h"
#include "src/common/parse_time.h"
#include "src/common/uid.h"

#define P_NAME "PrEp-pika: "
#define BUFF_LEN 128
#define MAX_STR_LEN 10240 /* 10 KB */
#define LOG_BUFF_LEN 512

const char plugin_name[] = "PrEp plugin pika";
const char plugin_type[] = "prep/pika";
const uint32_t plugin_version = SLURM_VERSION_NUMBER;

static bool have_prolog_slurmctld = false;
static bool have_epilog_slurmctld = false;

void (*prolog_slurmctld_callback)(int rc, uint32_t job_id, bool timed_out) = NULL;
void (*epilog_slurmctld_callback)(int rc, uint32_t job_id, bool timed_out) = NULL;

char *prefix = "JOB_RECORD__";
char *exclude_keys_prolog = "";
char *exclude_keys_epilog = "";
static pthread_mutex_t plugin_log_lock = PTHREAD_MUTEX_INITIALIZER;
int slurmctld_log_level;

/********************
 * connection infos *
 ********************/
typedef struct
{
    uint32_t port;
    const char *host;
    const char *vhost;
    const char *exchange;
    const char *user_name;
    const char *passwd;
} conn_info_t;

typedef struct
{
    char *prolog;
    char *epilog;
} routing_key_t;

typedef struct
{
    char *message;
    char *message_type;
    char *routing_key;
} rabbitmq_arg_t;

conn_info_t conn;

routing_key_t routing_key = {
    "prolog",
    "epilog"};

/**************
 * plugin log *
 **************/
typedef struct
{
    char *path;
    FILE *fd;
    bool set;
} log_t;

log_t plugin_log;

/**
 * Log on info level. Logs to costom log file, if so configured.
 */
#define PREP_INFO(_f, ...)                               \
    {                                                    \
        if (plugin_log.set)                              \
            prep_log(LOG_LEVEL_INFO, _f, ##__VA_ARGS__); \
        else                                             \
            slurm_info(_f, ##__VA_ARGS__);               \
    }

/**
 * Log on error level. Logs to costom log file, if so configured.
 */
#define PREP_ERROR(_f, ...)                               \
    {                                                     \
        if (plugin_log.set)                               \
            prep_log(LOG_LEVEL_ERROR, _f, ##__VA_ARGS__); \
        else                                              \
            slurm_error(_f, ##__VA_ARGS__);               \
    }

/**
 * Log on verbose level. Logs to costom log file, if so configured.
 */
#define PREP_VERBOSE(_f, ...)                               \
    {                                                       \
        if (plugin_log.set)                                 \
            prep_log(LOG_LEVEL_VERBOSE, _f, ##__VA_ARGS__); \
        else                                                \
            slurm_verbose(_f, ##__VA_ARGS__);               \
    }

/**
 * Log on debug level. Logs to costom log file, if so configured.
 */
#define PREP_DEBUG(_f, ...)                               \
    {                                                     \
        if (plugin_log.set)                               \
            prep_log(LOG_LEVEL_DEBUG, _f, ##__VA_ARGS__); \
        else                                              \
            slurm_debug(_f, ##__VA_ARGS__);               \
    }

/**
 * Log on debug2 level. Logs to costom log file, if so configured.
 */
#define PREP_DEBUG2(_f, ...)                               \
    {                                                      \
        if (plugin_log.set)                                \
            prep_log(LOG_LEVEL_DEBUG2, _f, ##__VA_ARGS__); \
        else                                               \
            slurm_debug2(_f, ##__VA_ARGS__);               \
    }

/**
 * Log on debug3 level. Logs to costom log file, if so configured.
 */
#define PREP_DEBUG3(_f, ...)                               \
    {                                                      \
        if (plugin_log.set)                                \
            prep_log(LOG_LEVEL_DEBUG3, _f, ##__VA_ARGS__); \
        else                                               \
            slurm_debug3(_f, ##__VA_ARGS__);               \
    }

typedef enum
{
    PROLOG = 0,
    EPILOG = 1
} context_t;

/**************************
 * json helper data types *
 **************************/
typedef enum
{
    SUCCESS = 0,
    E_JSON_OUT_OF_MEMORY = -2,
    E_HOST_LIST_SHIFT = -3,
    E_HOST_LIST_CREATE = -4,
    E_PREFIX_PREPEND = -5,
} internal_error_t;

typedef enum
{
    NONE = -1,
    U16,
    U32,
    U64,
    U16PP,
    CHARPP,
    CHARP,
    TIMEP,
} value_type_t;

typedef struct
{
    uint16_t **items;
    uint16_t *item_counts; // common type for sub count in slurm?
    uint32_t count;        // common type for over all count in slurm?
} uint16_2d_t;

typedef struct
{
    char **items;
    int count;
} char_2d_t;

typedef union
{
    uint16_t uint16;
    uint32_t uint32;
    uint64_t uint64;
    char *charp;
    char_2d_t charpp;
    uint16_2d_t uint16pp;
    time_t *timep;
} value_t;

typedef struct
{
    job_record_t *job_ptr;
    char *job_name;
    uint64_t mem_per_cpu;
    uint64_t mem_per_node;
    uint16_t **ids;
    uint16_t *id_cnts;
    char **node_names;
    int name_cnt;
    char *user;
    char *job_state;
    uint32_t gpu_alloc_cnt;
    uint16_t smt_enabled;
    char *job_script;
    uint32_t core_cnt;
} prep_t;

typedef struct key_value_pair key_value_pair_t;
struct key_value_pair
{
    char *key;
    value_type_t type;
    void (*get_value)(key_value_pair_t *, prep_t *);
    value_t value;
    bool collect[2];
};

key_value_pair_t *collect;

/*************************
 * function declarations *
 *************************/
/* Added spank_like output functions for convenience */
extern void slurm_info(const char *format, ...)
    __attribute__((format(printf, 1, 2)));
extern void slurm_error(const char *format, ...)
    __attribute__((format(printf, 1, 2)));
extern void slurm_verbose(const char *format, ...)
    __attribute__((format(printf, 1, 2)));
extern void slurm_debug(const char *format, ...)
    __attribute__((format(printf, 1, 2)));
extern void slurm_debug2(const char *format, ...)
    __attribute__((format(printf, 1, 2)));
extern void slurm_debug3(const char *format, ...)
    __attribute__((format(printf, 1, 2)));

/* Forward declerations */
static void *rabbitmq_send(void *args_ptr);
static int job_ptr_to_json(job_record_t *job_ptr, char **json, context_t cntxt);
static const char *error_to_string(internal_error_t error);
static int copy_key_value_pairs(key_value_pair_t *dst,
                                key_value_pair_t *src,
                                size_t size);
static void prep_log(log_level_t log_level, const char *fmt, ...)
    __attribute__((format(printf, 2, 3)));

/* Helper macro.
 * Uses _expr (usually an expression returning a number/error)
 * to either advance an offset value _o by that number or
 * return the number as error _e if it is negative */
#define ADVANCE_OR_RETURN(_o, _e, _expr) \
    {                                    \
        _e = _expr;                      \
        if (_e < 0)                      \
            return _e;                   \
        else                             \
            _o += _e;                    \
    }

/* Function macro.
 * Provide a body for the key_value_pair_t get_value callback.
 * The injected parameter names:
 * - key_value_pair_t: kvp
 * - prep_t: prep
 */
#define VALUE_CB(fn_name, body)                       \
    void fn_name(key_value_pair_t *kvp, prep_t *prep) \
    {                                                 \
        do                                            \
        {                                             \
            body                                      \
        } while (0);                                  \
    }

VALUE_CB(kv_array_id, kvp->value.uint32 = prep->job_ptr->array_job_id;)
VALUE_CB(kv_job_name, kvp->value.charp = prep->job_name;)
VALUE_CB(kv_user_id, kvp->value.uint32 = prep->job_ptr->user_id;)
VALUE_CB(kv_account, kvp->value.charp = prep->job_ptr->account;)
VALUE_CB(kv_work_dir, kvp->value.charp = prep->job_ptr->details->work_dir;)
VALUE_CB(kv_partition, kvp->value.charp = prep->job_ptr->partition;)
VALUE_CB(kv_account_value, kvp->value.charp = prep->job_ptr->account;)
VALUE_CB(kv_cpu_cnt, kvp->value.uint32 = prep->job_ptr->job_resrcs->ncpus;)
VALUE_CB(kv_cpu_ids, kvp->value.uint16pp.items = prep->ids;
         kvp->value.uint16pp.item_counts = prep->id_cnts;
         kvp->value.uint16pp.count = prep->job_ptr->job_resrcs->nhosts;)
VALUE_CB(kv_node_cnt, kvp->value.uint32 = prep->job_ptr->job_resrcs->nhosts;)
VALUE_CB(kv_node_names, kvp->value.charpp.items = prep->node_names;
         kvp->value.charpp.count = prep->name_cnt;)
VALUE_CB(kv_mem_per_cpu, kvp->value.uint64 = prep->mem_per_cpu;)
VALUE_CB(kv_mem_per_node, kvp->value.uint64 = prep->mem_per_node;)
VALUE_CB(kv_time_limit, kvp->value.uint32 = prep->job_ptr->time_limit;)
VALUE_CB(kv_whole_node, kvp->value.uint16 = prep->job_ptr->details->whole_node;)
VALUE_CB(kv_comment, kvp->value.charp = prep->job_ptr->comment;)
VALUE_CB(kv_start_time, kvp->value.timep = &(prep->job_ptr->start_time);)
VALUE_CB(kv_start_unixtime, kvp->value.uint64 = prep->job_ptr->start_time;)
VALUE_CB(kv_user, kvp->value.charp = prep->user;)
VALUE_CB(kv_partition_name, kvp->value.charp = prep->job_ptr->part_ptr->name;)
VALUE_CB(kv_billable_cores, kvp->value.uint32 = (int)(prep->job_ptr->billable_tres);)
VALUE_CB(kv_smt_enabled, kvp->value.uint16 = prep->smt_enabled;)
VALUE_CB(kv_gpu_alloc_cnt, kvp->value.uint32 = prep->gpu_alloc_cnt;)
VALUE_CB(kv_submit_time, kvp->value.timep = &(prep->job_ptr->details->submit_time);)
VALUE_CB(kv_submit_unixtime, kvp->value.uint64 = prep->job_ptr->details->submit_time;)
VALUE_CB(kv_end_time, kvp->value.timep = &(prep->job_ptr->end_time);)
VALUE_CB(kv_end_unixtime, kvp->value.uint64 = prep->job_ptr->end_time;)
VALUE_CB(kv_job_state, kvp->value.charp = prep->job_state;)
VALUE_CB(kv_gres_detail_str, kvp->value.charpp.items = prep->job_ptr->gres_detail_str;
         kvp->value.charpp.count = prep->job_ptr->gres_detail_cnt;)
VALUE_CB(kv_job_script, kvp->value.charp = prep->job_script;)
VALUE_CB(kv_core_cnt, kvp->value.uint32 = prep->core_cnt;)
VALUE_CB(kv_spank_job_env, kvp->value.charpp.items = prep->job_ptr->spank_job_env;
         kvp->value.charpp.count = prep->job_ptr->spank_job_env_size;)

/*
    debug conn_info_t with specified print function
*/
void conn_info_t__debug(conn_info_t *conn, int print_fn(const char *, ...))
{
    print_fn("host: %s\n", conn->host);
    print_fn("vhost: %s\n", conn->vhost);
    print_fn("port: %d\n", conn->port);
    print_fn("username: %s\n", conn->user_name);
    print_fn("password: %s\n", conn->passwd);
    print_fn("exchange: %s\n", conn->exchange);
}

void conn_info_t__free(conn_info_t *conn)
{
    xfree(*conn->host);
    xfree(*conn->vhost);
    xfree(*conn->passwd);
    xfree(*conn->user_name);
    xfree(*conn->exchange);
}

const char *deep_copy_string(const char *original)
{
    size_t original_len = strlen(original) + 1;
    // +1 because strlen doesnt count \0

    char *copy = xcalloc(original_len, sizeof(char));
    strncpy(copy, original, original_len);

    return copy;
}

/*
    read .ini config file and save entries to conn
*/
void conn_info_t__allocate_from_config(conn_info_t *conn, const char *ini_config_file_path)
{
    dictionary *config = iniparser_load(ini_config_file_path);

    const char *rabbitmq_host = iniparser_getstring(config, "credentials:rabbitmq_host", "not-found");
    const char *rabbitmq_vhost = iniparser_getstring(config, "credentials:vhost", "not-found");
    int rabbitmq_port = iniparser_getint(config, "credentials:rabbitmq_port", -1);
    const char *rabbitmq_username = iniparser_getstring(config, "credentials:rabbitmq_username", "not-found");
    const char *rabbitmq_password = iniparser_getstring(config, "credentials:rabbitmq_password", "not-found");
    const char *rabbitmq_exchange = iniparser_getstring(config, "credentials:exchange_name", "not-found");

    conn->host = deep_copy_string(rabbitmq_host);
    conn->vhost = deep_copy_string(rabbitmq_vhost);
    conn->port = rabbitmq_port;
    conn->user_name = deep_copy_string(rabbitmq_username);
    conn->passwd = deep_copy_string(rabbitmq_password);
    conn->exchange = deep_copy_string(rabbitmq_exchange);

    iniparser_freedict(config);
}

/*
    return path of rabbitmq.ini
*/
const char *get_config_file_path()
{
    slurm_conf_t *cf = NULL;
    cf = slurm_conf_lock();
    // get slurm configuration variables

    char *config_path;
    if (cf == NULL)
    {
        /*
        use default configuration file path if slurm fails to give us access to
        slurm configuration

        put configuration file on heap that the caller can always call free()
        */
        const char *default_config_file_path = "/etc/slurm/rabbitmq.ini";
        slurm_info(P_NAME "cant find slurm config path, defaulting to: %s\n",
                   default_config_file_path);
        config_path = (char *)deep_copy_string(default_config_file_path);
    }
    else
    {
        char *slurm_config_path = cf->slurm_conf;
        slurm_info(P_NAME "found slurm.conf path: %s\n", slurm_config_path);
        /*
        the slurm config path is a path to the slurm configuration,
        not our rabbitmq config

        our actual target is the file called rabbitmq.ini located in the same
        folder

        this way we need to do some string pointer math
        */
        char *last_slash_location = strrchr(slurm_config_path, '/');
        size_t directory_string_len =
            (last_slash_location - slurm_config_path) + 1;

        char *config_file = "rabbitmq.ini";
        size_t config_file_len = strlen(config_file) + 1;

        config_path =
            xcalloc(directory_string_len + config_file_len, sizeof(char));

        strncpy(config_path, slurm_config_path, directory_string_len);
        strncpy(&config_path[directory_string_len], config_file,
                config_file_len);
    }

    slurm_conf_unlock();

    return config_path;
}

/**************
 * plugin api *
 **************/
extern int init(void)
{
    slurm_info(P_NAME "init\n");

    const char *config_file_path = get_config_file_path();
    slurm_info(P_NAME "using %s as config file", config_file_path);
    conn_info_t__allocate_from_config(&conn, config_file_path);
    xfree(config_file_path);

    key_value_pair_t collectables[] = {
        {"ARRAY_ID", U32, kv_array_id},
        {"JOB_NAME", CHARP, kv_job_name},
        {"USER_ID", U32, kv_user_id},
        {"ACCOUNT", CHARP, kv_account},
        {"WORK_DIR", CHARP, kv_work_dir},
        {"PARTITION", CHARP, kv_partition},
        {"CPU_CNT", U32, kv_cpu_cnt},
        {"CPU_IDS", U16PP, kv_cpu_ids},
        {"NODE_CNT", U32, kv_node_cnt},
        {"NODE_NAMES", CHARPP, kv_node_names},
        {"MEM_PER_CPU", U64, kv_mem_per_cpu},
        {"MEM_PER_NODE", U64, kv_mem_per_node},
        {"TIME_LIMIT", U32, kv_time_limit},
        {"WHOLE_NODE", U16, kv_whole_node},
        {"COMMENT", CHARP, kv_comment},
        {"START_TIME", TIMEP, kv_start_time},
        {"USER", CHARP, kv_user},
        {"PARTITION_NAME", CHARP, kv_partition_name},
        {"BILLABLE_CORES", U32, kv_billable_cores},
        {"SMT_ENABLED", U16, kv_smt_enabled},
        {"GPU_ALLOC_CNT", U32, kv_gpu_alloc_cnt},
        {"SUBMIT_TIME", TIMEP, kv_submit_time},
        {"END_TIME", TIMEP, kv_end_time},
        {"JOB_STATE", CHARP, kv_job_state},
        {"GRES_DETAIL_STR", CHARPP, kv_gres_detail_str},
        {"JOB_SCRIPT", CHARP, kv_job_script},
        {"CORE_CNT", U32, kv_core_cnt},
        {"SPANK_JOB_ENV", CHARPP, kv_spank_job_env},
        {"SUBMIT_UNIXTIME", U64, kv_submit_unixtime},
        {"START_UNIXTIME", U64, kv_start_unixtime},
        {"END_UNIXTIME", U64, kv_end_unixtime},

        {NULL, NONE},
    };
    size_t size = sizeof(collectables) / sizeof(key_value_pair_t);
    slurmctld_log_level = get_sched_log_level();

    collect = xmalloc(sizeof(key_value_pair_t) * size);
    int rc = copy_key_value_pairs(collect, collectables, size);
    if (rc)
    {
        slurm_error(P_NAME "%s", error_to_string(rc));
        return SLURM_ERROR;
    }
    if (plugin_log.path)
    {
        plugin_log.fd = fopen(plugin_log.path, "a+");
        if (!plugin_log.fd)
        {
            slurm_error(
                P_NAME "Failed to open %s: %s",
                plugin_log.path,
                strerror(errno));
            return SLURM_ERROR;
        }
        plugin_log.set = true;
    }
    return SLURM_SUCCESS;
}

extern void fini(void)
{
    conn_info_t__free(&conn);

    key_value_pair_t *ptr = collect;
    if (ptr)
    {
        while (ptr->key)
        {
            xfree(ptr->key);
        }
        xfree(collect);
    }
    if (plugin_log.fd)
    {
        fclose(plugin_log.fd);
    }
}

extern void prep_p_register_callbacks(prep_callbacks_t *callbacks)
{
    /*
     * Cannot safely run these without a valid callback, so disable
     * them.
     */

    if (!(prolog_slurmctld_callback = callbacks->prolog_slurmctld))
        have_prolog_slurmctld = false;
    if (!(epilog_slurmctld_callback = callbacks->epilog_slurmctld))
        have_epilog_slurmctld = false;
}

extern int prep_p_prolog(job_env_t *job_env, slurm_cred_t *cred)
{

    return SLURM_SUCCESS;
}

extern int prep_p_epilog(job_env_t *job_env, slurm_cred_t *cred)
{

    return SLURM_SUCCESS;
}

extern int prep_p_prolog_slurmctld(job_record_t *job_ptr, bool *async)
{
    int rc = SLURM_SUCCESS;
    rabbitmq_arg_t *args = xmalloc(sizeof(rabbitmq_arg_t));
    internal_error_t _rc = job_ptr_to_json(job_ptr, &(args->message), PROLOG);
    args->message_type = "application/json";
    args->routing_key = "prolog";
    if (_rc)
    {
        PREP_ERROR(P_NAME "%s", error_to_string(_rc));
        rc = SLURM_ERROR;
    }
    *async = have_prolog_slurmctld;
    if (*async)
    {
        prolog_slurmctld_callback(rc, job_ptr->job_id, true);
    }
    if (rc == SLURM_SUCCESS)
    {
        slurm_thread_create_detached(NULL, rabbitmq_send, args);
    }
    return rc;
}

extern int prep_p_epilog_slurmctld(job_record_t *job_ptr, bool *async)
{
    int rc = SLURM_SUCCESS;
    rabbitmq_arg_t *args = xmalloc(sizeof(rabbitmq_arg_t));
    internal_error_t _rc = job_ptr_to_json(job_ptr, &(args->message), EPILOG);
    args->message_type = "application/json";
    args->routing_key = "epilog";
    if (_rc)
    {
        PREP_ERROR(P_NAME "%s", error_to_string(_rc));
        rc = SLURM_ERROR;
    }
    *async = have_prolog_slurmctld;
    if (*async)
    {
        prolog_slurmctld_callback(rc, job_ptr->job_id, true);
    }
    if (rc == SLURM_SUCCESS)
    {
        slurm_thread_create_detached(NULL, rabbitmq_send, args);
    }
    return rc;
}

extern void prep_p_required(prep_call_type_t type, bool *required)
{
    *required = false;
    switch (type)
    {
    case PREP_PROLOG_SLURMCTLD:
        if (running_in_slurmctld())
            *required = true;
        break;
    case PREP_EPILOG_SLURMCTLD:
        if (running_in_slurmctld())
            *required = true;
        break;
    case PREP_PROLOG:
    case PREP_EPILOG:
        if (running_in_slurmd())
            *required = true;
        break;
    default:
        return;
    }

    return;
}

/**
 * Return string representation of error.
 */
static const char *error_to_string(internal_error_t error)
{
    switch (error)
    {
    case SUCCESS:
        return "success";
    case E_JSON_OUT_OF_MEMORY:
        return "ran out of memory when building json string";
    case E_HOST_LIST_SHIFT:
    case E_HOST_LIST_CREATE:
        return "failed to retrieve node names";
    case E_PREFIX_PREPEND:
        return "failed to prepend prefix to keys";
    default:
        return "unknown error";
    }
}

/**
 * Infer per cpu or per node cpus usage. Unused values will be set to NO_VAL64.
 */
static void get_memory(job_record_t *job_ptr, prep_t *preps)
{
    uint64_t pn_min_memory = job_ptr->details->pn_min_memory;
    preps->mem_per_cpu = NO_VAL64;
    preps->mem_per_node = NO_VAL64;
    if (pn_min_memory & MEM_PER_CPU)
    {
        preps->mem_per_cpu = pn_min_memory & ~MEM_PER_CPU;
    }
    else
    {
        preps->mem_per_node = pn_min_memory;
    }
}

/**
 * Read the job resource core bitmap to infer cpu ids.
 */
static int get_cpu_ids(job_record_t *job_ptr, prep_t *prep)
{
    int rc = SLURM_SUCCESS;
    job_resources_t *job_resrcs = job_ptr->job_resrcs;
    uint16_t ncores = 0;
    uint32_t nhosts = job_resrcs->nhosts;
    uint32_t reps_remain = 0;
    int h, c, b, sock_ind;
    bitoff_t offset;
    bitstr_t *bitmap = job_resrcs->core_bitmap;

    prep->ids = xmalloc(sizeof(uint16_t *) * nhosts);
    prep->id_cnts = xmalloc(sizeof(uint16_t) * nhosts);
    for (h = 0, sock_ind = 0, offset = 0; h < nhosts && !rc; ++h)
    {
        if (reps_remain == 0)
        {
            reps_remain = job_resrcs->sock_core_rep_count[sock_ind];
            ncores = job_resrcs->sockets_per_node[sock_ind] *
                     job_resrcs->cores_per_socket[sock_ind];
            ++sock_ind;
        }
        --reps_remain;
        prep->ids[h] = xmalloc(sizeof(uint16_t) * ncores);
        for (c = 0, b = 0; c < ncores && b < ncores; ++b)
        {
            if (bit_test(bitmap, b + offset))
            {
                prep->ids[h][c++] = b;
            }
        }
        prep->id_cnts[h] = c;
        offset += ncores;
    }
    prep->core_cnt = 0;
    for (h = 0; h < nhosts; ++h)
    {
        prep->core_cnt += prep->id_cnts[h];
    }
    return rc;
}

/**
 * Get node names as array by splitting the job resource nodes string.
 */
static int get_nodes_names(job_record_t *job_ptr, prep_t *prep)
{
    int rc = 0, i;
    char *host;
    hostlist_t hl;
    if ((hl = slurm_hostlist_create(job_ptr->job_resrcs->nodes)))
    {
        prep->name_cnt = slurm_hostlist_count(hl);
        prep->node_names = xmalloc(sizeof(char *) * prep->name_cnt);
        for (i = 0; i < prep->name_cnt && !rc; ++i)
        {
            if ((host = slurm_hostlist_shift(hl)))
            {
                prep->node_names[i] = host;
            }
            else
            {
                rc = E_HOST_LIST_SHIFT;
            }
        }
        hostlist_destroy(hl);
        return rc;
    }
    return E_HOST_LIST_CREATE;
}

static void free_nodes_names(char **node_names, int count)
{
    int i;
    char *item;
    for (i = 0; i < count; ++i)
    {
        item = node_names[i];
        if (item)
        {
            free(item);
        }
    }
    xfree(node_names);
}

/**
 * Get current job state.
 */
static void get_job_state(job_record_t *job_ptr, prep_t *prep)
{
    prep->job_state = (char *)xmalloc(10 * sizeof(char));
    if (IS_JOB_RUNNING(job_ptr))
        sprintf(prep->job_state, "running");
    else if (IS_JOB_COMPLETE(job_ptr))
        sprintf(prep->job_state, "completed");
    else if (IS_JOB_CANCELLED(job_ptr))
        sprintf(prep->job_state, "cancelled");
    else if (IS_JOB_TIMEOUT(job_ptr))
        sprintf(prep->job_state, "timeout");
    else if (IS_JOB_OOM(job_ptr))
        sprintf(prep->job_state, "OOM");
    else
        sprintf(prep->job_state, "failed");
}

/**
 * Get total number of allocated GPUs.
 */
static int get_gpu_number(job_record_t *job_ptr, prep_t *prep)
{
    int rc = SLURM_SUCCESS;

    // check gpu_alloc_cnt
    if (job_ptr->gres_used != NULL)
    {
        char *gres_alloc = strdup(job_ptr->gres_used);
        char *ptr = strtok(gres_alloc, "gpu:");
        while (ptr != NULL)
        {
            prep->gpu_alloc_cnt = atoi(ptr);
            ptr = strtok(NULL, "gpu:");
        }
        free(ptr);
        free(gres_alloc);
    }
    return rc;
}

/**
 * Print string at offset to a xmalloced string.
 * Memory will be resized as needed.
 */
static int xsnprintf_realloc(char **json, size_t offset, const char *fmt, ...)
{
    va_list args;
    size_t size = xsize(*json);
    int rc;
    va_start(args, fmt);
    rc = vsnprintf(*json + offset, size - offset, fmt, args);
    va_end(args);
    if (rc >= size - offset)
    {
        do
        {
            if (size >= SIZE_MAX / 2)
            {
                return E_JSON_OUT_OF_MEMORY;
            }
            size *= 2;
        } while (rc >= size - offset);
        *json = xrealloc(*json, sizeof(char) * size);
        va_start(args, fmt);
        rc = vsnprintf(*json + offset, size - offset, fmt, args);
        va_end(args);
    }
    return rc;
}

/**
 * Prepend prefix to string and xmallocs new memory with provided size.
 */
static int xprepend_prefix(char **dst, char *src, size_t size)
{
    char buffer[size];
    int rc = snprintf(buffer, size, "%s%s", prefix, src);
    *dst = xstrndup(buffer, size);
    return rc;
}

static int json_init(char **json, size_t offset, uint32_t job_id)
{
    return xsnprintf_realloc(json, offset, "{\"%" PRIu32 "\":{", job_id);
}

static int
json_append_uint16(char **json, size_t offset, const char *key, uint16_t value)
{
    return xsnprintf_realloc(json, offset, "\"%s\": %" PRIu16 ",", key, value);
}

static int
json_append_uint32(char **json, size_t offset, const char *key, uint32_t value)
{
    return xsnprintf_realloc(json, offset, "\"%s\": %" PRIu32 ",", key, value);
}

static int
json_append_uint64(char **json, size_t offset, const char *key, uint64_t value)
{
    return xsnprintf_realloc(json, offset, "\"%s\": %" PRIu64 ",", key, value);
}

static int
json_append_uint16_array_2d(char **json, size_t offset, const char *key,
                            uint16_t **values,
                            uint16_t *value_counts,
                            uint32_t count)
{
    int i, j;
    int error = 0;
    int rc = 0;
    ADVANCE_OR_RETURN(rc, error, xsnprintf_realloc(json, offset, "\"%s\": [", key));
    for (i = 0; i < count; ++i)
    {
        ADVANCE_OR_RETURN(rc, error, xsnprintf_realloc(json, offset + rc, "["));
        for (j = 0; j < value_counts[i]; ++j)
        {
            ADVANCE_OR_RETURN(rc, error, xsnprintf_realloc(json, offset + rc, "%i,", values[i][j]));
        }
        if (j > 0)
        {
            (*json)[strlen(*json) - 1] = '\0';
            --rc;
        }
        ADVANCE_OR_RETURN(rc, error, xsnprintf_realloc(json, offset + rc, "],"));
    }
    if (i > 0)
    {
        (*json)[strlen(*json) - 1] = '\0';
        --rc;
    }
    ADVANCE_OR_RETURN(rc, error, xsnprintf_realloc(json, offset + rc, "],"));
    return rc;
}

static int
json_append_string_array(char **json, size_t offset, const char *key,
                         char **values, int count)
{
    int rc = 0, error = 0;
    int i;
    ADVANCE_OR_RETURN(rc, error, xsnprintf_realloc(json, offset, "\"%s\": [", key));
    for (i = 0; i < count; ++i)
    {
        ADVANCE_OR_RETURN(rc, error, xsnprintf_realloc(json, offset + rc, "\"%s\",", values[i]));
    }
    if (i > 0)
    {
        (*json)[offset + rc] = '\0';
        --rc;
    }
    ADVANCE_OR_RETURN(rc, error, xsnprintf_realloc(json, offset + rc, "],"));
    return rc;
}

static int
json_append_string(char **json, size_t offset, const char *key,
                   const char *value)
{
    return xsnprintf_realloc(json, offset, "\"%s\": \"%s\",", key, value);
}

static int json_append_time(char **json, size_t offset, const char *key,
                            time_t *time_ptr)
{
    char time_str[32];
    slurm_make_time_str(time_ptr, time_str, 32);
    return xsnprintf_realloc(json, offset, "\"%s\": \"%s\",", key, time_str);
}

static int json_fini(char **json, bool has_data)
{
    size_t offstet = strlen(*json);
    if (has_data)
    {
        (*json)[offstet - 1] = '}';
        return xsnprintf_realloc(json, offstet, "%c", '}');
    }
    else
    {
        return xsnprintf_realloc(json, offstet, "%s", "}}");
    }
}

static int
json_append_key_value_pair(char **json, size_t offset, key_value_pair_t *kvp)
{
    switch (kvp->type)
    {
    case U16:
        return json_append_uint16(
            json, offset, kvp->key,
            kvp->value.uint16);
    case U32:
        return json_append_uint32(
            json, offset, kvp->key,
            kvp->value.uint32);
    case U64:
        return json_append_uint64(
            json, offset, kvp->key,
            kvp->value.uint64);
    case CHARPP:
        return json_append_string_array(
            json, offset, kvp->key,
            kvp->value.charpp.items,
            kvp->value.charpp.count);
    case CHARP:
        return json_append_string(
            json, offset, kvp->key,
            kvp->value.charp);
    case U16PP:
        return json_append_uint16_array_2d(
            json, offset, kvp->key,
            kvp->value.uint16pp.items,
            kvp->value.uint16pp.item_counts,
            kvp->value.uint16pp.count);
    case TIMEP:
        return json_append_time(
            json, offset, kvp->key, kvp->value.timep);
    default:
        return xsnprintf_realloc(
            json, offset,
            "\"%s\":\"data type not recognised\",", kvp->key);
    }
}

static bool
key_required(const char *key, bool *all, bool *collect)
{
    collect[PROLOG] = all[PROLOG] || (key && !strstr(exclude_keys_prolog, key));
    collect[EPILOG] = all[EPILOG] || (key && !strstr(exclude_keys_epilog, key));
    return collect[PROLOG] || collect[EPILOG];
}

/**
 * Copy key value pairs and preped prefix to keys, which will be xmalloced or
 * nulled on error.
 * Returns SUCCESS or E_PREFIX_PREPEND;
 */
static internal_error_t
copy_key_value_pairs(key_value_pair_t *dst, key_value_pair_t *src, size_t size)
{
    int rc = SLURM_SUCCESS;
    int i;
    bool all[] = {
        strlen(exclude_keys_prolog) == 0,
        strlen(exclude_keys_epilog) == 0,
    };
    bool collect[2];
    key_value_pair_t *dst_ptr = dst;
    key_value_pair_t *src_ptr = src;
    for (i = 0; i < size && rc >= 0; ++i, ++src_ptr)
    {
        if (key_required(src_ptr->key, all, collect))
        {
            *dst_ptr = *src_ptr;
            dst_ptr->collect[PROLOG] = collect[PROLOG];
            dst_ptr->collect[EPILOG] = collect[EPILOG];
            if (src_ptr->key)
            {
                dst_ptr->key = NULL;
                rc = xprepend_prefix(&(dst_ptr->key), src_ptr->key, BUFF_LEN);
            }
            ++dst_ptr;
        }
    }
    return rc >= 0 ? SUCCESS : E_PREFIX_PREPEND;
}

static char _convert_dec_hex(char x)
{
    if (x <= 9)
        x += '0';
    else
        x += 'A' - 10;

    return x;
}

/* Escape characters according to RFC7159 and ECMA-262 11.8.4.2 */
static char *_json_escape(const char *str)
{
    char *ret = NULL;
    int i, o, len;

    len = strlen(str) * 2 + 128;
    ret = xmalloc(len);
    for (i = 0, o = 0; str[i]; ++i)
    {
        if (o >= MAX_STR_LEN)
        {
            break;
        }
        else if ((o + 8) >= len)
        {
            len *= 2;
            ret = xrealloc(ret, len);
        }
        switch (str[i])
        {
        case '\\':
            ret[o++] = '\\';
            ret[o++] = '\\';
            break;
        case '"':
            ret[o++] = '\\';
            ret[o++] = '\"';
            break;
        case '\n':
            ret[o++] = '\\';
            ret[o++] = 'n';
            break;
        case '\b':
            ret[o++] = '\\';
            ret[o++] = 'b';
            break;
        case '\f':
            ret[o++] = '\\';
            ret[o++] = 'f';
            break;
        case '\r':
            ret[o++] = '\\';
            ret[o++] = 'r';
            break;
        case '\t':
            ret[o++] = '\\';
            ret[o++] = 't';
            break;
            break;
        case '/':
            ret[o++] = '\\';
            ret[o++] = '/';
            break;
        default:
            /* use hex for all other control characters */
            if (str[i] <= 0x1f || str[i] == '\'' || str[i] == '<' ||
                str[i] == 0x5C)
            {
                ret[o++] = '\\';
                ret[o++] = 'u';
                ret[o++] = '0';
                ret[o++] = '0';
                ret[o++] =
                    _convert_dec_hex((0xf0 & str[i]) >> 4);
                ret[o++] = _convert_dec_hex(0x0f & str[i]);
            }
            else /* normal character */
                ret[o++] = str[i];
        }
    }
    return ret;
}

static internal_error_t
job_ptr_to_json(job_record_t *job_ptr, char **json, context_t context)
{
    key_value_pair_t *kvp = collect;
    internal_error_t rc = SUCCESS;
    prep_t prep[1] = {{.job_ptr = job_ptr}};
    int h;
    size_t max_len = BUFF_LEN;
    size_t offset = 0;
    bool has_data = false;
    buf_t *script;

    get_memory(job_ptr, prep);
    if ((rc = get_nodes_names(job_ptr, prep)) ||
        (rc = get_cpu_ids(job_ptr, prep)))
    {
        return rc;
    }
    // Get user name
    prep->user = uid_to_string_or_null(job_ptr->user_id);
    // Check if multithreading is requested (this is a workaround)
    prep->smt_enabled = 0;
    if (job_ptr->details->mc_ptr->threads_per_core == NO_VAL16)
        prep->smt_enabled = 1;
    // Get job state
    get_job_state(job_ptr, prep);
    // determine total GPU number
    if ((rc = get_gpu_number(job_ptr, prep)))
        return rc;

    // Escape characters according to RFC7159 and ECMA-262 11.8.4.2
    prep->job_name = _json_escape(job_ptr->name);

    script = get_job_script(job_ptr);
    if (script)
    {
        prep->job_script = _json_escape(script->head);
    }

    *json = xmalloc(sizeof(char) * max_len);

    rc = json_init(json, 0, job_ptr->job_id);
    offset += rc;
    while (kvp && kvp->key && rc >= 0)
    {
        if (kvp->collect[context])
        {
            has_data = true;
            kvp->get_value(kvp, prep);
            rc = json_append_key_value_pair(json, offset, kvp);
            if (rc >= 0)
            {
                offset += rc;
            }
        }
        ++kvp;
    }
    if (rc >= 0)
    {
        rc = json_fini(json, has_data);
    }

    free_nodes_names(prep->node_names, prep->name_cnt);
    for (h = 0; h < job_ptr->job_resrcs->nhosts; ++h)
    {
        xfree(prep->ids[h]);
    }
    xfree(prep->ids);
    xfree(prep->id_cnts);
    xfree(prep->user);
    xfree(prep->job_state);
    xfree(prep->job_name);
    if (script)
        xfree(prep->job_script);
    free_buf(script);
    return rc >= 0 ? SUCCESS : rc;
}

static void log_status_error(const char *routing_key, amqp_status_enum status)
{
    PREP_ERROR(
        P_NAME "%s: amqp_status: (-0x%04X) %s",
        routing_key,
        -status,
        amqp_error_string2(status));
}

static void log_reply_error(const char *routing_key, amqp_rpc_reply_t reply)
{
    char *exception;
    switch (reply.reply_type)
    {
    case AMQP_RESPONSE_NONE:
        exception = "AMQP_RESPONSE_NONE";
        PREP_ERROR(
            P_NAME "%s: amqp_reply: (%s): %s",
            routing_key,
            exception,
            "missing response");
        break;
    case AMQP_RESPONSE_LIBRARY_EXCEPTION:
        exception = "AMQP_RESPONSE_LIBRARY_EXCEPTION";
        PREP_ERROR(
            P_NAME "%s: amqp_reply: (%s): (-0x%04X) %s",
            routing_key,
            exception,
            -reply.library_error,
            amqp_error_string2(reply.library_error));
        break;
    case AMQP_RESPONSE_SERVER_EXCEPTION:
        exception = "AMQP_RESPONSE_SERVER_EXCEPTION";
        switch (reply.reply.id)
        {
        case AMQP_CONNECTION_CLOSE_METHOD:
        {
            amqp_connection_close_t *m = reply.reply.decoded;
            PREP_ERROR(
                P_NAME "%s: amqp_reply: (%s): %" PRIu16 ", %.*s",
                routing_key,
                exception,
                m->reply_code,
                (int)m->reply_text.len,
                (char *)m->reply_text.bytes);
            break;
        }
        case AMQP_CHANNEL_CLOSE_METHOD:
        {
            amqp_channel_close_t *m = reply.reply.decoded;
            PREP_ERROR(
                P_NAME "%s: amqp_reply: (%s): %" PRIu16 ", %.*s",
                routing_key,
                exception,
                m->reply_code,
                (int)m->reply_text.len,
                (char *)m->reply_text.bytes);
            break;
        }
        default:
            PREP_ERROR(
                P_NAME "%s: amqp_reply: (%s): %s",
                routing_key,
                exception,
                "unknown server error");
            break;
        }
        break;
    case AMQP_RESPONSE_NORMAL:
        exception = "AMQP_RESPONSE_NORMAL";
        PREP_ERROR(
            P_NAME "%s: amqp_reply: (%s): %s",
            routing_key,
            exception,
            "response normal");
        break;
    default:
        exception = "???";
        PREP_ERROR(
            P_NAME "%s: amqp_reply: (%s): %s",
            routing_key,
            exception,
            "unknown response (broker running?)");
        break;
    }
}

static void *rabbitmq_send(void *args_ptr)
{
    amqp_status_enum status = AMQP_STATUS_OK;
    amqp_rpc_reply_t reply;
    amqp_socket_t *socket = NULL;
    amqp_connection_state_t state;
    amqp_basic_properties_t props;
    rabbitmq_arg_t *args = (rabbitmq_arg_t *)args_ptr;

    /* publish properties */
    props._flags = AMQP_BASIC_CONTENT_TYPE_FLAG | AMQP_BASIC_DELIVERY_MODE_FLAG;
    props.content_type = amqp_cstring_bytes(args->message_type);
    props.delivery_mode = 1; /* non persistent delivery mode */

    if (!args)
    {
        PREP_ERROR(P_NAME "rabbitmq_send: %s", "arguments missing");
        return NULL;
    }

    state = amqp_new_connection();
    socket = amqp_tcp_socket_new(state);
    if (!socket)
    {
        PREP_ERROR(P_NAME "%s: amqp_socket: failed", args->routing_key);
        return NULL;
    }

    status = amqp_socket_open(socket, conn.host, conn.port);

    if (status == AMQP_STATUS_OK)
    {
        reply = amqp_login(
            state, // connection object
            conn.vhost,
            0,                      // max channel limit set to unlimited
            131072,                 // recommended max frame AMQP size
            0,                      // heartbeat disabled
            AMQP_SASL_METHOD_PLAIN, // login method, requires uname and upass
            conn.user_name,
            conn.passwd);
    }

    if (status == AMQP_STATUS_OK && reply.reply_type == AMQP_RESPONSE_NORMAL)
    {
        amqp_channel_open(state, 1);
        reply = amqp_get_rpc_reply(state);
    }

    if (status == AMQP_STATUS_OK && reply.reply_type == AMQP_RESPONSE_NORMAL)
    {
        status = amqp_basic_publish(
            state,
            1,
            amqp_cstring_bytes(conn.exchange),
            amqp_cstring_bytes(args->routing_key),
            0,
            0,
            &props,
            amqp_cstring_bytes(args->message));
    }

    if (status == AMQP_STATUS_OK && reply.reply_type == AMQP_RESPONSE_NORMAL)
    {
        reply = amqp_channel_close(state, 1, AMQP_REPLY_SUCCESS);
    }
    if (status == AMQP_STATUS_OK && reply.reply_type == AMQP_RESPONSE_NORMAL)
    {
        reply = amqp_connection_close(state, AMQP_REPLY_SUCCESS);
    }
    if (status == AMQP_STATUS_OK && reply.reply_type == AMQP_RESPONSE_NORMAL)
    {
        status = amqp_destroy_connection(state);
    }
    if (status != AMQP_STATUS_OK)
    {
        log_status_error(args->routing_key, status);
    }
    if (reply.reply_type != AMQP_RESPONSE_NORMAL)
    {
        log_reply_error(args->routing_key, reply);
    }
    xfree(args->message);
    xfree(args);
    return NULL;
}

static const char *log_level_string(log_level_t log_level)
{
    switch (log_level)
    {
    case LOG_LEVEL_INFO:
        return "info";
    case LOG_LEVEL_ERROR:
        return "error";
    case LOG_LEVEL_VERBOSE:
        return "verbose";
    case LOG_LEVEL_DEBUG:
        return "debug";
    case LOG_LEVEL_DEBUG2:
        return "debug2";
    case LOG_LEVEL_DEBUG3:
        return "debug3";
    default:
        return "unknown level";
    }
}

static void prep_log(log_level_t log_level, const char *fmt, ...)
{
    FILE *fd = plugin_log.fd;
    int src, frc;
    va_list args;
    char log_buffer[LOG_BUFF_LEN];
    char timestamp[32];

    if (log_level > slurmctld_log_level)
        return;

    log_timestamp(timestamp, 32);

    va_start(args, fmt);
    src = vsnprintf(log_buffer, LOG_BUFF_LEN, fmt, args);

    slurm_mutex_lock(&plugin_log_lock);
    frc = fprintf(
        fd, "[%s] %s: %s\n", timestamp, log_level_string(log_level), log_buffer);
    if (src < 0 || frc < 0)
    {
        slurm_error(
            P_NAME "Log error (vsnprintf rc: %d, fprintf rc: %d)",
            src,
            frc);
    }
    fflush(fd);
    slurm_mutex_unlock(&plugin_log_lock);
    va_end(args);
}
