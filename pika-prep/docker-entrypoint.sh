#!/bin/bash

set -e

_build_prep_plugin_rpm() {
    cd /root
    tar -czvf slurm-prep-pika-plugin-${SLURM_VERSION}.tar.gz slurm-prep-pika-plugin-${SLURM_VERSION}
    rpmbuild --define="_topdir `pwd`/rpmbuild" -ta slurm-prep-pika-plugin-${SLURM_VERSION}.tar.gz
    echo "Copy RPM files to host."
    cp rpmbuild/RPMS/$(uname -m)/slurm-prep-pika-plugin* /RPMS
}

_build_prep_plugin_rpm

exec "${@}"
