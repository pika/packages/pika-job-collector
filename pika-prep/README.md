This component creates the PrEp plugin to capture detailed job metadata.

- [Create PrEp plugin RPM](#create-prep-plugin-rpm)
- [Install PrEp plugin on Slurm controller node](#install-prep-plugin-on-slurm-controller-node)
- [Configure Slurm controller](#configure-slurm-controller)
- [Configure Slurm compute nodes](#configure-slurm-compute-nodes)
- [Configure pika-server](#configure-pika-server)

---

# Create PrEp plugin RPM

Build the Docker image

```sh
ROCKY_VERSION=8.9 SLURM_VERSION=23.11.8 make
```

**Note**: Only RockyLinux versions 8.x and 9.x are supported.

Build the PrEp plugin

```sh
make install
```

If the build was successful the resulting RPM package should be placed under RPMS.

# Install PrEp plugin on Slurm controller node

```sh
dnf localinstall -y slurm-prep-pika-plugin-*.rpm
```

# Configure Slurm controller

Make sure that the [RabbitMQ server](https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-rabbitmq) is running and that the Slurm controller node has access to it.

Adapt your `job_submit.lua` script accordingly to `/etc/pika/job_submit.lua.pika`. This gives HPC users the option of switching off PIKA monitoring. This script only allows monitoring to be switched off if the `--exclusive` flag has been set.

Adjust `/etc/pika/rabbitmq.ini.example` and copy it to the same directory where `slurm.conf` is located.

Example:

```sh
cd /etc/slurm
cp /etc/pika/rabbitmq.ini.example rabbitmq.ini

vi rabbitmq.ini
[credentials]
rabbitmq_host = pika-rabbitmq
vhost = test
rabbitmq_port = 5672
rabbitmq_username = pika
rabbitmq_password = pika123
exchange_name = exchange.job_events
```

Adjust slurm.conf and restart the slurm controller.

Example:

```sh
vi slurm.conf

JobSubmitPlugins=lua
NodeName=...Features=no_monitoring
PrEpPlugins=pika

# restart slurm controller
systemctl restart slurmctld
```

After restarting the Slurm Controller, the metadata of the jobs should now be intercepted and forwarded to RabbitMQ.

You can verify this with our RabbitMQ consumer script.
This script is written in python and requires the pika package.
You can install it via pip:

```sh
pip3 install pika

python3 /etc/pika/receive_rabbitmq.py
  [*] Waiting for messages. To exit press CTRL+C
```

# Configure Slurm compute nodes

Switching off the monitoring at the request of the HPC user takes place in the prolog script of each compute node.
The `pika-collectd` rpm already contains the prolog script in `/etc/pika/prolog.sh`.

Make sure that the prolog.sh script is used in slurm.conf and restart the Slurm daemon.

```sh
vi slurm.conf

Prolog=/etc/pika/prolog.sh
```

```sh
systemctl restart slurmd
```

# Configure pika-server

Connect to the machine on which `pika-server` is running and adjust `pika.conf` to enable data
collection.

```sh
vi pika.conf

[[data-collections]]
rabbitmq_hostname      = ""
rabbitmq_vhost         = ""
rabbitmq_exchange      = "exchange.job_events"
rabbitmq_exchange_type = "fanout"
rabbitmq_queue         = "queue.job_events.pika"
rabbitmq_mode_list     = ["prolog", "epilog"]
rabbitmq_username      = ""
rabbitmq_password      = ""
queue_arguments        = {}

```
