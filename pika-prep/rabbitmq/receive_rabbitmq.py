#!/usr/bin/env python3
import configparser
import json
import os
import sys
from pprint import pprint

import pika


def main():
    # Create a ConfigParser instance
    config = configparser.ConfigParser()

    # Read the conf file
    config.read("/etc/pika/rabbitmq.ini")

    # RabbitMQ server connection parameters
    rabbitmq_host = config["credentials"]["rabbitmq_host"]
    vhost = config["credentials"]["vhost"]
    rabbitmq_port = config["credentials"]["rabbitmq_port"]
    rabbitmq_username = config["credentials"]["rabbitmq_username"]
    rabbitmq_password = config["credentials"]["rabbitmq_password"]
    exchange_name = config["credentials"]["exchange_name"]

    # Create a connection to RabbitMQ server
    credentials = pika.PlainCredentials(
        username=rabbitmq_username, password=rabbitmq_password
    )
    connection_params = pika.ConnectionParameters(
        host=rabbitmq_host,
        port=rabbitmq_port,
        virtual_host=vhost,
        credentials=credentials,
    )
    connection = pika.BlockingConnection(connection_params)
    channel = connection.channel()

    channel.exchange_declare(
        exchange=exchange_name, exchange_type="fanout", durable=True
    )

    result = channel.queue_declare(queue="queue.job_events.pika", durable=True)
    queue_name = result.method.queue

    channel.queue_bind(exchange=exchange_name, queue=queue_name)

    def callback(ch, method, properties, body):
        mode = method.routing_key
        print(mode)
        pprint(json.loads(body))

    channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)

    print(" [*] Waiting for messages. To exit press CTRL+C")
    channel.start_consuming()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Interrupted")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
