# Job metadata collector

PIKA provides two options for collecting job metadata.

The first option is only suitable for node-exclusive jobs in which the compute nodes receive the job metadata in prolog and epilog by [Slurm tools](#simple-installation-based-on-slurm-tools) and send it to the PIKA database.

The second option is based on a [PrEp plugin](#advanced-installation-based-on-prep-plugin) on the Slurm controller node, which captures detailed job metadata, such as the CPU/GPU lists for node-sharing jobs.

## Simple installation based on Slurm tools

This setup uses the Slurm job environment variables and the `scontrol` tool to capture the job metadata.

The following components must be installed as an RPM and rolled out to each compute node.

- [pika-spank](pika-spank) - Slurm plugin to control job monitoring.
- [pika-control](pika-control) - Global PIKA configuration files and the slurm integration for prolog and epilog.

## Advanced installation based on PrEp plugin

This setup requires a [RabbitMQ server](https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-rabbitmq) that temporarily stores the job metadata.

We provide a Docker container that is used to create an RPM for the PrEp plugin for the following Slurm versions:

- 23.02.7
- 23.11.1
- 23.11.3
- 23.11.8
- 23.11.9
- 24.05.2

Only these versions have been tested by us. Newer Slurm versions that are not listed here can lead to problems. We always try to provide the PrEp plugin for the latest Slurm version.

Please follow the instructions in [pika-prep](pika-prep).
