#!/bin/bash

cd /src/pika-control
make rpm
cp -r rpmbuild/RPMS/$(uname -m)/* /rpmbuild/

cd /src/pika-spank
make rpm
cp -r rpmbuild/RPMS/$(uname -m)/* /rpmbuild/

exec "$@"